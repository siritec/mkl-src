use std::env::var;
use std::path::*;
use std::io::*;

fn main() {
    //let out_dir = PathBuf::from(var("OUT_DIR").unwrap_or("/opt/intel/mkl/lib/".to_owned()));
    let mkl_dir = PathBuf::from("/opt/intel/mkl/lib/intel64_lin".to_owned());
    let lib_dir = PathBuf::from("/opt/intel/lib/intel64_lin".to_owned());

    println!("cargo:rustc-link-search={}", mkl_dir.display());
    println!("cargo:rustc-link-search={}", lib_dir.display());
    println!("cargo:rustc-link-lib=mkl_intel_ilp64");
    println!("cargo:rustc-link-lib=mkl_intel_thread");
    println!("cargo:rustc-link-lib=mkl_core");
    //println!("cargo:rustc-link-lib=mkl_tbb_thread");
    println!("cargo:rustc-link-lib=iomp5");
    println!("cargo:rustc-link-lib=m");
}
